package com.lcz;


import com.alibaba.alicloud.sms.SmsServiceImpl;
import com.aliyun.dysmsapi20170525.models.SendSmsRequest;
import com.aliyuncs.dysmsapi.model.v20170525.QuerySendDetailsResponse;
import com.aliyuncs.dysmsapi.model.v20170525.SendSmsResponse;
import com.aliyuncs.exceptions.ClientException;
import com.lcz.util.SmsUtil;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class SendSmsDemo {

    @Test
    public void testSendSms(){
        System.out.println("----------测试短信发送--------");
        try {
            SendSmsResponse response= SmsUtil.sendVerifyCode("17150408878", "123456","SMS_147418961");
            System.out.println("短信接口返回的数据----------------");
            System.out.println("Code=" + response.getCode());
            System.out.println("Message=" + response.getMessage());
            System.out.println("RequestId=" + response.getRequestId());
            System.out.println("BizId=" + response.getBizId());//消息回执id

            Thread.sleep(3000L);
            // 查明细
            if (response.getCode() != null && response.getCode().equals("OK")) {
                System.out.println("-------------------查看短息发送状态--------------");

                QuerySendDetailsResponse querySendDetailsResponse = SmsUtil.querySendDetails("17150408878", response.getBizId());
                System.out.println("短信明细查询接口返回数据----------------");
                System.out.println("Code=" + querySendDetailsResponse.getCode());
                System.out.println("Message=" + querySendDetailsResponse.getMessage());
                int i = 0;
                for (QuerySendDetailsResponse.SmsSendDetailDTO smsSendDetailDTO : querySendDetailsResponse
                        .getSmsSendDetailDTOs()) {
                    System.out.println("SmsSendDetailDTO[" + i + "]:");
                    System.out.println("Content=" + smsSendDetailDTO.getContent());
                    System.out.println("ErrCode=" + smsSendDetailDTO.getErrCode());
                    System.out.println("OutId=" + smsSendDetailDTO.getOutId());
                    System.out.println("PhoneNum=" + smsSendDetailDTO.getPhoneNum());
                    System.out.println("ReceiveDate=" + smsSendDetailDTO.getReceiveDate());
                    System.out.println("SendDate=" + smsSendDetailDTO.getSendDate());
                    System.out.println("SendStatus=" + smsSendDetailDTO.getSendStatus());
                    System.out.println("Template=" + smsSendDetailDTO.getTemplateCode());
                }
                System.out.println("TotalCount=" + querySendDetailsResponse.getTotalCount());
                System.out.println("RequestId=" + querySendDetailsResponse.getRequestId());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Autowired
    private SmsServiceImpl smsService;

    @Test
    public void testSendSms2() throws ClientException {
//        SmsRequest
        // 组装请求对象-具体描述见控制台-文档部分内容
        com.aliyuncs.dysmsapi.model.v20170525.SendSmsRequest request = new com.aliyuncs.dysmsapi.model.v20170525.SendSmsRequest();
        // 必填:待发送手机号
        request.setPhoneNumbers("17150408878");
        // 必填:短信签名-可在短信控制台中找到
        request.setSignName("E点餐管家");
        // 必填:短信模板-可在短信控制台中找到
        request.setTemplateCode("SMS_147418961");
        // 可选:模板中的变量替换JSON串,如模板内容为"亲爱的${name},您的验证码为${code}"时,此处的值为
        request.setTemplateParam("{code:" + "999999" + "}");
        SendSmsResponse  response=smsService.sendSmsRequest(request);
        System.out.println("短信接口返回的数据----------------");
        System.out.println("Code=" + response.getCode());
        System.out.println("Message=" + response.getMessage());
        System.out.println("RequestId=" + response.getRequestId());
        System.out.println("BizId=" + response.getBizId());//消息回执id
    }
}