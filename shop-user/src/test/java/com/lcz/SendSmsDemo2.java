package com.lcz;


import com.alibaba.alicloud.sms.SmsServiceImpl;
import com.aliyuncs.dysmsapi.model.v20170525.SendBatchSmsRequest;
import com.aliyuncs.dysmsapi.model.v20170525.SendBatchSmsResponse;
import com.aliyuncs.dysmsapi.model.v20170525.SendSmsRequest;
import com.aliyuncs.dysmsapi.model.v20170525.SendSmsResponse;
import com.aliyuncs.exceptions.ClientException;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class SendSmsDemo2 {



    @Autowired
    private SmsServiceImpl smsService;

    @Test
    public void testSendSms2() throws ClientException {
//        SmsRequest
        // 组装请求对象-具体描述见控制台-文档部分内容
        SendSmsRequest request = new SendSmsRequest();
        // 必填:待发送手机号
        request.setPhoneNumbers("17150408878");
        // 必填:短信签名-可在短信控制台中找到
        request.setSignName("E点餐管家");
        // 必填:短信模板-可在短信控制台中找到
        request.setTemplateCode("SMS_147418961");
        // 可选:模板中的变量替换JSON串,如模板内容为"亲爱的${name},您的验证码为${code}"时,此处的值为
        request.setTemplateParam("{code:" + "999999" + "}");
        SendSmsResponse  response=smsService.sendSmsRequest(request);
        System.out.println("短信接口返回的数据----------------");
        System.out.println("Code=" + response.getCode());
        System.out.println("Message=" + response.getMessage());
        System.out.println("RequestId=" + response.getRequestId());
        System.out.println("BizId=" + response.getBizId());//消息回执id


    }

    @Test
    public void testSendBatchSms() throws ClientException {
        System.out.println("--------批量短信发送-------");
        //批量短信发送
        SendBatchSmsRequest sendBatchSmsRequest = new SendBatchSmsRequest();
        sendBatchSmsRequest.setPhoneNumberJson("[\"17150408878\",\"13602498395\"]");
        sendBatchSmsRequest.setTemplateCode("SMS_147418961");
        sendBatchSmsRequest.setSignNameJson("[\"E点餐管家\",\"E点餐管家\"]");
        sendBatchSmsRequest.setTemplateParamJson("[{\"code\":\"111111\"},{\"code\":\"666666\"}]");

        SendBatchSmsResponse response=smsService.sendSmsBatchRequest(sendBatchSmsRequest);
        System.out.println("短信接口返回的数据----------------");
        System.out.println("Code=" + response.getCode());
        System.out.println("Message=" + response.getMessage());
        System.out.println("RequestId=" + response.getRequestId());
        System.out.println("BizId=" + response.getBizId());//消息回执id
    }
}