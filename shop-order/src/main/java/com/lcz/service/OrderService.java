package com.lcz.service;

import com.lcz.pojo.Order;

public interface OrderService {

    public Order createOrder(Order order);
}
