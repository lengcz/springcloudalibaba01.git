package com.lcz.service;

import com.lcz.pojo.Product;
import com.lcz.service.fallback.ProductServiceFallback;
import com.lcz.service.fallback.ProductServiceFallbackFactory;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * value用于指定调用的nacos下哪个微服务<br>
 * fallback用于指定当前feign接口容错类
 */
//@FeignClient(value="server-product",fallback = ProductServiceFallback.class)
@FeignClient(value="server-product"
//        ,fallbackFactory = ProductServiceFallbackFactory.class
)
public interface ProductService {

    @RequestMapping("/product/{pid}")
    Product findById(@PathVariable  Integer pid);

    /**
     * 扣库存
     * @param pid 商品id
     * @param num 扣除数量
     */
    @RequestMapping("/product/reduceInventory")
    void reduceInventory(@RequestParam("pid") Integer pid,
                         @RequestParam("num") Integer num);
}

