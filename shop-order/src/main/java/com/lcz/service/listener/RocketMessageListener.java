package com.lcz.service.listener;

import lombok.extern.slf4j.Slf4j;
import org.apache.rocketmq.spring.annotation.ConsumeMode;
import org.apache.rocketmq.spring.annotation.MessageModel;
import org.apache.rocketmq.spring.annotation.RocketMQMessageListener;
import org.apache.rocketmq.spring.core.RocketMQListener;
import org.springframework.stereotype.Component;

/**
 * 监听器
 */
@Component
@RocketMQMessageListener(
        topic = "create-order",//消费主题
        consumerGroup = "group_rocketmq", //消费者分组
        consumeMode = ConsumeMode.CONCURRENTLY,//消费模式: 无序CONCURRENTLY和有序ORDERLY
        messageModel = MessageModel.CLUSTERING //消费模式: 广播和集群,默认集群模式
)
@Slf4j
public class RocketMessageListener implements RocketMQListener<String> {

    @Override
    public void onMessage(String id) {
        log.info("监听到订单号,id ："+id);
    }
}
