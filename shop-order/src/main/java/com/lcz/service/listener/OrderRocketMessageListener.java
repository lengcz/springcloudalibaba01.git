package com.lcz.service.listener;

import com.lcz.pojo.Order;
import lombok.extern.slf4j.Slf4j;
import org.apache.rocketmq.spring.annotation.RocketMQMessageListener;
import org.apache.rocketmq.spring.core.RocketMQListener;
import org.springframework.stereotype.Component;

/**
 * 监听器
 */
@Component
@RocketMQMessageListener(
        topic = "order-topic",//消费主题
        consumerGroup = "group_rocketmq" //消费者分组
)
@Slf4j
public class OrderRocketMessageListener implements RocketMQListener<Order> {

    @Override
    public void onMessage(Order order) {
        System.out.println("-----监听到订单-----");
       log.info("监听到用户下单成功，向用户发送待支付通知"+ order);
    }
}
