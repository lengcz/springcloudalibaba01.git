package com.lcz.service.impl;

import com.alibaba.csp.sentinel.annotation.SentinelResource;
import com.alibaba.csp.sentinel.slots.block.BlockException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class MySourceServiceImpl {

    /**
     * 定义一个资源<br>
     * 定义当资源内部发生异常的时候的处理逻辑<br>
     * blockHandler 定义当资源内部发生了BlockException应该进入的方法,捕获的是Sentinel定义的异常<br>
     * fallback     定义当资源内部发生了Throwable应该进入的方法
     * @param name
     * @return
     */
    @SentinelResource(value="findById",
            blockHandlerClass = MySourceServiceImplBlockHandler.class,
            blockHandler = "blockHandler11",
            fallbackClass = MySourceServiceImplFallback.class,
            fallback = "fallback2")//定义资源
    public String findById(String name){
        log.info("查询处理");

//        int a=1/0;
        log.info("查询处理结束");
        return "abc";
    }

//    /**
//     * 定义一个资源<br>
//     * 定义当资源内部发生异常的时候的处理逻辑<br>
//     * blockHandler 定义当资源内部发生了BlockException应该进入的方法,捕获的是Sentinel定义的异常<br>
//     * fallback     定义当资源内部发生了Throwable应该进入的方法
//     * @param name
//     * @return
//     */
//    @SentinelResource(value="findById",blockHandler = "blockHandler11",fallback = "fallback2")//定义资源
//    public String findById(String name){
//        log.info("查询处理");
//
//        int a=1/0;
//        log.info("查询处理结束");
//        return "abc";
//    }

    /**
     * blockHanler要求<br>
     *     1. 当前方法的返回值和参数必须与原方法保持一致<br>
     *     2. 允许在参数列表的最后加入一个参数BlockException，用来接受原方法中发生的异常
     * @param name
     * @param e
     * @return
     */
    public String blockHandler11(String name, BlockException e) throws Exception{
        log.error("发生BlockException时，被运行了");
        return "BlockException";
    }

    /**
     * fallback2<br>
     *     1. 当前方法的返回值和参数必须与原方法保持一致<br>
     *     2. 允许在参数列表的最后加入一个参数Throwable，用来接受原方法中发生的异常
     * @param name
     * @param e
     * @return
     */
    public String fallback2(String name, Throwable e){
        log.error("发生Throwable时，被运行了");
        return "Throwable";
    }
}
