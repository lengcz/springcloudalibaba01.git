package com.lcz.service.impl;

import com.lcz.dao.OrderDao;
import com.lcz.dao.TxLogDao;
import com.lcz.pojo.Order;
import com.lcz.pojo.TxLog;
import lombok.extern.slf4j.Slf4j;
import org.apache.rocketmq.spring.core.RocketMQTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.UUID;

@Service
@Slf4j
public class OrderServiceImpl4 {

    @Autowired
    private OrderDao orderDao;

    @Autowired
    private TxLogDao txLogDao;

    @Autowired
    private RocketMQTemplate rocketMQTemplate;

    public void createOrderBefore(Order order) {
        String uuid = UUID.randomUUID().toString();//设置唯一的事务id
        log.info("创建订单:"+uuid+",发送半事务消息");
        //发送半消息
        rocketMQTemplate.sendMessageInTransaction(
                "tx_topic",
                MessageBuilder.withPayload(order).setHeader("txId",uuid).build(),
                order);
    }

    @Transactional //本地事务,要么同时成功，要么同时失败
    public void createOrder(String txId,Order order) {
        log.info("创建订单:"+txId+",创建成功");
        //保存订单
        orderDao.save(order);
        TxLog txLog = new TxLog();
        txLog.setTxId(txId);
        txLog.setDate(new Date());
        //记录事务日志
        this.txLogDao.save(txLog);
    }
}
