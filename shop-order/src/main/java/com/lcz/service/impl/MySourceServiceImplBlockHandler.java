package com.lcz.service.impl;

import com.alibaba.csp.sentinel.annotation.SentinelResource;
import com.alibaba.csp.sentinel.slots.block.BlockException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Slf4j
public class MySourceServiceImplBlockHandler {
    /**
     * blockHanler要求<br>
     *     1. 当前方法的返回值和参数必须与原方法保持一致<br>
     *     2. 允许在参数列表的最后加入一个参数BlockException，用来接受原方法中发生的异常
     * @param name
     * @param e
     * @return
     */
    public static  String blockHandler11(String name, BlockException e) throws Exception{
        log.error("发生BlockException时，被运行了");
        return "BlockException";
    }
}
