package com.lcz.service.impl;

import com.alibaba.fastjson.JSON;
import com.lcz.dao.OrderDao;
import com.lcz.pojo.Order;
import com.lcz.pojo.Product;
import com.lcz.service.OrderService2;
import com.lcz.service.ProductService;
import io.seata.spring.annotation.GlobalTransactional;
import lombok.extern.slf4j.Slf4j;
import org.apache.rocketmq.spring.core.RocketMQTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class OrderService2Impl implements OrderService2 {

    @Autowired
    private OrderDao orderDao;

    @Autowired
    private ProductService productService;

    @Autowired
    private RocketMQTemplate rocketMQTemplate;

    @Override
    @GlobalTransactional //开启全局事务控制
    public Order createOrder(Integer pid, Integer num) {
        log.info("----------------调用商品服务，调用商品微服务查询此商品------------------------");

        /**
         * Ribbon负载均衡
         */
        Product product = productService.findById(pid);
        log.info("查询到的商品内容:" + JSON.toJSONString(product));

        Order order = new Order();
        order.setUid(1);
        order.setUsername("测试");
        order.setPid(pid);
        order.setPrice(product.getPrice());
        order.setPname(product.getPname());
        order.setNumber(num);

        orderDao.save(order);//本地事务
        log.info("用户下单成功,订单信息为:" + JSON.toJSON(order));

        log.info("扣减库存");
        //扣减库存
        productService.reduceInventory(pid,num);//远程事务

        //发送订单到消息队列
//        rocketMQTemplate.convertAndSend("order-topic",order);
        return order;
    }
}
