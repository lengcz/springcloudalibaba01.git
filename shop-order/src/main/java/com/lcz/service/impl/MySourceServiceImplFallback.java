package com.lcz.service.impl;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class MySourceServiceImplFallback {

    /**
     * fallback2<br>
     *     1. 当前方法的返回值和参数必须与原方法保持一致<br>
     *     2. 允许在参数列表的最后加入一个参数Throwable，用来接受原方法中发生的异常
     * @param name
     * @param e
     * @return
     */
    public static String fallback2(String name, Throwable e){
        log.error("发生Throwable时，被运行了");
        return "Throwable";
    }
}
