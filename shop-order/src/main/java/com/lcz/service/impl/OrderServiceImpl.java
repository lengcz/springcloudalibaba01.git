package com.lcz.service.impl;

import com.lcz.dao.OrderDao;
import com.lcz.pojo.Order;
import com.lcz.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class OrderServiceImpl implements OrderService {

    @Autowired
    private OrderDao orderDao;

    @Override
    public Order createOrder(Order order) {
        return orderDao.save(order);
    }
}
