package com.lcz.service.impl;


import com.lcz.dao.TxLogDao;
import com.lcz.pojo.Order;
import com.lcz.pojo.TxLog;
import lombok.extern.slf4j.Slf4j;
import org.apache.rocketmq.spring.annotation.RocketMQTransactionListener;
import org.apache.rocketmq.spring.core.RocketMQLocalTransactionListener;
import org.apache.rocketmq.spring.core.RocketMQLocalTransactionState;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.Message;
import org.springframework.stereotype.Service;


@Service
@RocketMQTransactionListener
@Slf4j
public class OrderServiceImpl4Listener implements RocketMQLocalTransactionListener {

    @Autowired
    private OrderServiceImpl4 orderServiceImpl4;

    @Autowired
    private TxLogDao txLogDao;

    //执行本地事务
    @Override
    public RocketMQLocalTransactionState executeLocalTransaction(Message message, Object o) {
        //第一个参数message对应的消息，第二个参数对应的arg
        String txId = (String)message.getHeaders().get("txId");
        try{
            log.info("执行事务，txId:"+txId);
            Order order=(Order) o;
            orderServiceImpl4.createOrder(txId,order);
            log.info("执行事务成功，txId:"+txId);
            return RocketMQLocalTransactionState.COMMIT; //本地事务成功
        }catch (Exception e){
            log.info("执行事务失败，txId:"+txId);
            return RocketMQLocalTransactionState.ROLLBACK; //本地事务失败
        }
    }

    //消息回查
    @Override
    public RocketMQLocalTransactionState checkLocalTransaction(Message message) {
        String txId = (String)message.getHeaders().get("txId");
        log.info("事务回查，txId:"+txId);
       TxLog txLog= txLogDao.findById(txId).get();
       if(null!=txLog){
           //本地事务成功了
           return RocketMQLocalTransactionState.COMMIT;
       }
       return RocketMQLocalTransactionState.ROLLBACK;
    }
}
