package com.lcz.service.fallback;

import com.lcz.pojo.Product;
import com.lcz.service.ProductService;
import feign.hystrix.FallbackFactory;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * 容错类,需要实现Feign所在的接口，并去实现接口中的所有方法,一旦Feign远程调用出现问题了，就会进入当前类的同名方法，执行容错逻辑
 */
@Service
@Slf4j
public class ProductServiceFallbackFactory implements FallbackFactory<ProductService> {

    @Override
    public ProductService create(Throwable throwable) {
        log.error("======"+throwable.getMessage());
        return new ProductService() {
            @Override
            public Product findById(Integer pid) {
                Product product=new Product();
                product.setPid(-1);
                product.setPname("FallbackFactory远程调用微服务异常，进入容错");
                return product;
            }

            @Override
            public void reduceInventory(Integer pid, Integer num) {

            }
        };
    }
}
