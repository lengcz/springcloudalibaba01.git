package com.lcz.service.fallback;

import com.lcz.pojo.Product;
import com.lcz.service.ProductService;
import org.springframework.stereotype.Service;

/**
 * 容错类,需要实现Feign所在的接口，并去实现接口中的所有方法,一旦Feign远程调用出现问题了，就会进入当前类的同名方法，执行容错逻辑
 */
//@Service
public class ProductServiceFallback implements ProductService {
    @Override
    public Product findById(Integer pid) {

        Product product=new Product();
        product.setPid(-1);
        product.setPname("远程调用微服务异常，进入容错");
        return product;
    }

    @Override
    public void reduceInventory(Integer pid, Integer num) {

    }
}
