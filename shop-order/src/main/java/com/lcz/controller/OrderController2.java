package com.lcz.controller;

import com.alibaba.fastjson.JSON;
import com.lcz.pojo.Order;
import com.lcz.pojo.Product;
import com.lcz.service.OrderService;
import com.lcz.service.ProductService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import java.util.concurrent.TimeUnit;


/**
 * 服务容错示例
 */
@RestController
@Slf4j
public class OrderController2 {


    @Autowired
    private ProductService productService;

    @GetMapping("/order2/prod/{pid}")
    public String  order(@PathVariable Integer pid) {
        log.info("调用商品服务，调用商品微服务查询此商品");
//        try {
//            TimeUnit.SECONDS.sleep(2);
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        }

        /**
         * Ribbon负载均衡
         */
        Product product = productService.findById(pid);
        log.info("查询到的商品内容:" + JSON.toJSONString(product));
        return "order";
    }

    @GetMapping("/order2/hello")
    public String hello() {
        log.info("say hello------------");
        return "hello";
    }

}
