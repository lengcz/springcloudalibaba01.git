package com.lcz.controller;

import com.alibaba.fastjson.JSON;
import com.lcz.pojo.Order;
import com.lcz.pojo.Product;
import com.lcz.service.OrderService;
import com.lcz.service.ProductService;
import com.lcz.service.impl.OrderServiceImpl4;
import com.lcz.util.DateUtils;
import com.lcz.util.IPUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.rocketmq.client.producer.SendCallback;
import org.apache.rocketmq.client.producer.SendResult;
import org.apache.rocketmq.common.message.Message;
import org.apache.rocketmq.spring.core.RocketMQTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import javax.servlet.http.HttpServletRequest;
import java.net.SocketException;


@RestController
@Slf4j
public class OrderController {

    @Autowired
    private OrderServiceImpl4 orderService;

    @Autowired
    private RestTemplate restTemplate;

    @Autowired
    private DiscoveryClient discoveryClient;

    @Autowired
    private ProductService productService;

    @Autowired
    private RocketMQTemplate rocketMQTemplate;


    @GetMapping("/order/create")
    public String createOrder() {
        long id=System.currentTimeMillis();//订单号
        log.info("创建订单:"+id);
        Message message=new Message("create-order",(id+"").getBytes());
        message.setDelayTimeLevel(5);//设置延时等级
        rocketMQTemplate.sendOneWay("create-order",message);
//        rocketMQTemplate.asyncSend("create-order:创建订单", message, new SendCallback() {
//            @Override
//            public void onSuccess(SendResult sendResult) {
//                System.out.println("成功回调消息");
//                System.out.println(sendResult);
//            }
//            @Override
//            public void onException(Throwable throwable) {
//                System.out.println("回调发生异常了");
//                System.out.println(throwable);
//            }
//        });

        return "success";
    }

    @GetMapping("/order/prod/{pid}")
    public Order order(@PathVariable Integer pid, HttpServletRequest request) throws SocketException {
        log.info("调用商品服务，调用商品微服务查询此商品");
        log.info("request ip:"+ IPUtil.getIpAddr(request));
        log.info("local ip:"+IPUtil.getRealIp());
        /**
         * Ribbon负载均衡
         */
        Product product = productService.findById(pid);

        if(product.getPid()<1){
            Order order=new Order();
            order.setOid(-1L);
            order.setPname("下单失败");
            return order;
        }

        log.info("查询到的商品内容:" + JSON.toJSONString(product));

        Order order = new Order();
        order.setUid(1);
        order.setUsername("测试");
        order.setPid(pid);
        order.setPrice(product.getPrice());
        order.setPname(product.getPname());
        order.setNumber(1);

        orderService.createOrderBefore(order);
        log.info("用户下单成功,订单信息为:" + JSON.toJSON(order));

        return order;
    }

//    @GetMapping("/order/prod/{pid}")
//    public Order order(@PathVariable Integer pid) {
//        log.info("调用商品服务，调用商品微服务查询此商品");
//
//        /**
//         * Ribbon负载均衡
//         */
//        Product product = restTemplate.getForObject("http://server-product/product/" + pid, Product.class);
//        log.info("查询到的商品内容:" + JSON.toJSONString(product));
//
//        Order order = new Order();
//        order.setUid(1);
//        order.setUsername("测试");
//        order.setPid(pid);
//        order.setPrice(product.getPrice());
//        order.setPname(product.getPname());
//        order.setNumber(1);
//
//        orderService.createOrder(order);
//        log.info("用户下单成功,订单信息为:" + JSON.toJSON(order));
//
//        return order;
//    }

//    @GetMapping("/order/prod/{pid}")
//    public Order order(@PathVariable Integer pid) {
//        log.info("调用商品服务，调用商品微服务查询此商品");
//
//        /**
//         * 手动从nacos获取服务列表，再手动调用的方式
//         */
//        ServiceInstance instance=discoveryClient.getInstances("server-product").get(0);
//        Product product = restTemplate.getForObject("http://"+instance.getHost()+":"+instance.getPort()+"/product/" + pid, Product.class);
//        log.info("查询到的商品内容:" + JSON.toJSONString(product));
//
//        Order order = new Order();
//        order.setUid(1);
//        order.setUsername("测试");
//        order.setPid(pid);
//        order.setPrice(product.getPrice());
//        order.setPname(product.getPname());
//        order.setNumber(1);
//
//        orderService.createOrder(order);
//        log.info("用户下单成功,订单信息为:" + JSON.toJSON(order));
//
//        return order;
//    }



//    @GetMapping("/order/prod/{pid}")
//    public Order order(@PathVariable Integer pid) {
//        log.info("调用商品服务，调用商品微服务查询此商品");
//
//        /**
//         * 无注册中心直接调用的方式
//         */
//        Product product = restTemplate.getForObject("http://localhost:8081/product/" + pid, Product.class);
//        log.info("查询到的商品内容:" + JSON.toJSONString(product));
//
//        Order order = new Order();
//        order.setUid(1);
//        order.setUsername("测试");
//        order.setPid(pid);
//        order.setPrice(product.getPrice());
//        order.setPname(product.getPname());
//        order.setNumber(1);
//
//        orderService.createOrder(order);
//        log.info("用户下单成功,订单信息为:" + JSON.toJSON(order));
//
//        return order;
//    }

}
