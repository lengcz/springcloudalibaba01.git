package com.lcz.controller;


import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
@Slf4j
public class ApiDemoController {


    @RequestMapping("/api1/demo1")
    public String demo1() {
        return "hello";
    }
    @RequestMapping("/api1/demo2")
    public String demo2() {
        return "hello";
    }
    @RequestMapping("/api2/demo1")
    public String demo3() {
        return "hello";
    }
    @RequestMapping("/api2/demo2")
    public String demo4() {
        return "hello";
    }
}
