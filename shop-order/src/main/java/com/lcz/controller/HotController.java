package com.lcz.controller;


import com.alibaba.csp.sentinel.annotation.SentinelResource;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
@Slf4j
public class HotController {

    @GetMapping("/hot")
    @SentinelResource("hotdemo")
    public String hello(String name,Integer id) {
        log.info("say hello------------1");
        return "name:"+name+"--id:"+id;
    }

}
