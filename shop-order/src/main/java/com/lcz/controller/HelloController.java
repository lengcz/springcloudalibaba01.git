package com.lcz.controller;


import com.lcz.service.impl.MySourceServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
@Slf4j
public class HelloController {



    @Autowired
    private MySourceServiceImpl mySourceService;

    @GetMapping("/hello1")
    public String hello() {
        log.info("say hello------------1");
        mySourceService.findById("b");
        return "hello1";
    }

    @GetMapping("/hello2")
    public String hello2() {
        log.info("say hello------------2");
        mySourceService.findById("a");
        return "hello2";
    }

    int i=0;
    @GetMapping("/hello3")
    public String hello3() {
        log.info("say hello------------3");
        //制造一个异常，每3次请求，抛出一次异常,异常比例1/3
        i++;
        if(i%3==0){
            throw new RuntimeException();
        }
        return "hello3";
    }

    @GetMapping("/hello4")
    public String hello4() {
        log.info("say hello------------1");
        return mySourceService.findById("b");
    }

}
