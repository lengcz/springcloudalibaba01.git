package com.lcz.controller;

import com.lcz.pojo.Order;
import com.lcz.service.OrderService2;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;


/**
 * seata处理事务
 */
@RestController
@Slf4j
public class OrderController3 {


    @Autowired
    private OrderService2 orderService2;

    @GetMapping("/order3/prod/{pid}")
    public Order order(@PathVariable Integer pid) {
        //这里直接写死了下单两个
        return orderService2.createOrder(pid,2);
    }
}
