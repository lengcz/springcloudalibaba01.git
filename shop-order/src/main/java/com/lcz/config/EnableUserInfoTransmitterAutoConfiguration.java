package com.lcz.config;

import com.lcz.userinfo.TransmitUserInfoFeighClientIntercepter;
import com.lcz.userinfo.TransmitUserInfoFilter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

//@Configuration //在业务端通过注解EanbleUserInfoTransmitter加载
public class EnableUserInfoTransmitterAutoConfiguration {

    public EnableUserInfoTransmitterAutoConfiguration() {
    }

    @Bean
    public TransmitUserInfoFeighClientIntercepter transmitUserInfo2FeighHttpHeader(){
        System.out.println("-----TransmitUserInfoFeighClientInterceptor");
        return new TransmitUserInfoFeighClientIntercepter();
    }

    @Bean
    public TransmitUserInfoFilter transmitUserInfoFromHttpHeader(){
        System.out.println("-----TransmitUserInfoFilter");
        return new TransmitUserInfoFilter();
    }
}
