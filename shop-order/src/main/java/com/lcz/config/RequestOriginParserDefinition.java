package com.lcz.config;

import com.alibaba.csp.sentinel.adapter.spring.webmvc.callback.RequestOriginParser;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;

@Component
public class RequestOriginParserDefinition implements RequestOriginParser {
    @Override
    public String parseOrigin(HttpServletRequest request) {

        //获取request的域，区分请求来源是pc还是app,mobile
        String name=request.getParameter("serviceName");
//        if(StringUtils.isEmpty(name)){
//            throw new RuntimeException("serviceName is empty");
//        }
//        System.out.println(name);
        return name;
    }
}
