package com.lcz.config;

/**
 * RocketMq消息延迟等级
 */
public class RocketMqMessageDelayLevelConstant {
    // https://github.com/apache/rocketmq/blob/master/store/src/main/java/org/apache/rocketmq/store/config/MessageStoreConfig.java
    //private String messageDelayLevel = "1s 5s 10s 30s 1m 2m 3m 4m 5m 6m 7m 8m 9m 10m 20m 30m 1h 2h";
    public final static int Level_1s=1;
    public final static int Level_5s=2;
    public final static int Level_10s=3;
    public final static int Level_30s=4;
    public final static int Level_1m=5;
    public final static int Level_2m=6;
    public final static int Level_3m=7;
    public final static int Level_4m=8;
    public final static int Level_5m=9;
    public final static int Level_6m=10;
    public final static int Level_7m=11;
    public final static int Level_8m=12;
    public final static int Level_9m=13;
    public final static int Level_10m=14;
    public final static int Level_20m=15;
    public final static int Level_30m=16;
    public final static int Level_1h=17;
    public final static int Level_2h=18;
}
