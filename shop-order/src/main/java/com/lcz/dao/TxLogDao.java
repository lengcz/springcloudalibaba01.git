package com.lcz.dao;

import com.lcz.pojo.TxLog;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TxLogDao extends JpaRepository<TxLog,String> {
}
