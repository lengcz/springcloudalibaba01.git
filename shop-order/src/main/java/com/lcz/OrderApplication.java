package com.lcz;
import com.lcz.config.EnableUserInfoTransmitter;
import org.apache.rocketmq.spring.autoconfigure.RocketMQProperties;
import org.apache.rocketmq.spring.core.RocketMQTemplate;
import org.apache.rocketmq.spring.support.RocketMQUtil;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;

@SpringBootApplication
@EnableDiscoveryClient //启动服务发现
@EnableFeignClients //开启feign的客户端
@EnableUserInfoTransmitter
public class OrderApplication {



    public static void main(String[] args) {

        ConfigurableApplicationContext context=SpringApplication.run(OrderApplication.class, args);
//       String[] names= context.getBeanDefinitionNames();
//       for(String name:names){
//           System.out.println(name);
//       }
        System.out.println(context.getEnvironment().getProperty("spring.application.name"));



        System.out.println(System.getProperty("user.home"));
    }

    @Bean
    @LoadBalanced //启动负载均衡
    public RestTemplate restTemplate(){
        return new RestTemplate();
    }
}