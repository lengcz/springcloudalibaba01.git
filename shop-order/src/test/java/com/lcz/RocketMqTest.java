package com.lcz;

import com.lcz.config.RocketMqMessageDelayLevelConstant;
import com.lcz.util.DateUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.rocketmq.client.consumer.DefaultMQPushConsumer;
import org.apache.rocketmq.client.consumer.listener.ConsumeConcurrentlyContext;
import org.apache.rocketmq.client.consumer.listener.ConsumeConcurrentlyStatus;
import org.apache.rocketmq.client.consumer.listener.MessageListenerConcurrently;
import org.apache.rocketmq.client.producer.DefaultMQProducer;
import org.apache.rocketmq.client.producer.SendResult;
import org.apache.rocketmq.common.message.Message;
import org.apache.rocketmq.common.message.MessageExt;
import org.junit.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;
import java.util.concurrent.TimeUnit;


@SpringBootTest
@Slf4j
public class RocketMqTest {

    @Test
    public void testProducer() throws Exception {
        // 创建消息生产者，设置生产者组名
        DefaultMQProducer producer = new DefaultMQProducer("myproducergroup");
        // 为生产者设置NameServer
        producer.setNamesrvAddr("127.0.0.1:9876");
        //启动生产者
        producer.start();
        //构建消息
        Message message = new Message("mytopic", "tag222", "hello world".getBytes());
        message.setDelayTimeLevel(RocketMqMessageDelayLevelConstant.Level_10s);//等级3标识10秒后才可以消费
        //发送消息
        SendResult result = producer.send(message, 5000);
        System.out.println("时间:" + DateUtils.getNow() + ",发送消息:" + result);
        //关闭生产者
        producer.shutdown();


    }

    @Test
    public void testConsumer() throws Exception {
        DefaultMQPushConsumer consumer = new DefaultMQPushConsumer("myconsumergroup");
        consumer.setNamesrvAddr("127.0.0.1:9876");
        consumer.subscribe("mytopic", "*");
        consumer.registerMessageListener(new MessageListenerConcurrently() {
            @Override
            public ConsumeConcurrentlyStatus consumeMessage(List<MessageExt> list, ConsumeConcurrentlyContext consumeConcurrentlyContext) {
                System.out.println("时间:" + DateUtils.getNow() + ",收到的消息:" + list);
                return ConsumeConcurrentlyStatus.CONSUME_SUCCESS;//返回状态是消费成功还是消费失败
            }
        });

        consumer.start();
        System.out.println("时间:" + DateUtils.getNow() + ",启动consumer成功");
        TimeUnit.HOURS.sleep(1);
    }
}
