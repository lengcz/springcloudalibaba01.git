package com.lcz;

import com.lcz.config.RocketMqMessageDelayLevelConstant;
import org.apache.rocketmq.client.producer.SendCallback;
import org.apache.rocketmq.client.producer.SendResult;
import org.apache.rocketmq.common.message.Message;
import org.apache.rocketmq.spring.core.RocketMQTemplate;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.concurrent.TimeUnit;

@RunWith(SpringRunner.class)
@SpringBootTest
public class MessageTypeTest {

    @Autowired
    private RocketMQTemplate rocketMQTemplate;

    @Test
    public void testSyncSend() {
//        SendResult result = rocketMQTemplate.syncSend("topic-1", "同步测试消息", 10000);
        //如果需要带偶tag标签，只要在topic后添加分号跟上tag名称即可
        SendResult result = rocketMQTemplate.syncSend("topic-1:mytag", "同步测试消息", 10000);

        System.out.println(result);
    }

    @Test
    public void testAsyncSend() throws Exception {
        rocketMQTemplate.asyncSend("topic-1:mytag", "异步测试消息", new SendCallback() {
            @Override
            public void onSuccess(SendResult sendResult) {
                System.out.println("成功回调消息");
                System.out.println(sendResult);
            }

            @Override
            public void onException(Throwable throwable) {
                System.out.println("回调发生异常了");
                System.out.println(throwable);
            }
        });


        System.out.println("-----------------");
        TimeUnit.SECONDS.sleep(300);//避免主线程死掉无法获取回调结果
    }

    @Test
    public void testSendOneWay(){
        //不回调，不等待
        rocketMQTemplate.sendOneWay("topic-1:mytag","异步单向消息");
    }

    @Test
    public void SendDelayMessage(){
        Message message=new Message("topic-2","延迟消息".getBytes());
//        MessageBuilder.withPayload()
        message.setDelayTimeLevel(RocketMqMessageDelayLevelConstant.Level_10s);
        rocketMQTemplate.sendOneWay("topic-1",message);
    }

    @Test
    public void testSendOneWayOrderly(){

        for(int j=0;j<8;j++){
            for (int i=0;i<10;i++){
                //第三个参数的作用是用来决定这些消息发送到哪个队列
                rocketMQTemplate.sendOneWayOrderly("topic-1","异步单向顺序消息2",j+"");
            }
        }

    }
}
