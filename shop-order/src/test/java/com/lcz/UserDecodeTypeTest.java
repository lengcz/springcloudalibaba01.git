package com.lcz;

import com.alibaba.fastjson.JSONObject;
import com.lcz.pojo.User;
import com.lcz.userinfo.UserInfo;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;


//@RunWith(SpringRunner.class)
@SpringBootTest
public class UserDecodeTypeTest {


    @Test
    public void testUser() throws UnsupportedEncodingException {
        UserInfo user=new UserInfo();
        user.setUid(111);
        user.setUsername("xiaowang");
        String str=  URLEncoder.encode(JSONObject.toJSONString(user),"UTF-8");
        System.out.println(str);
    }

}
