package com.lcz.controller;

import com.alibaba.fastjson.JSONObject;
import com.lcz.pojo.Product;
import com.lcz.service.ProductService;
import com.lcz.userinfo.UserInfoContext;
import com.lcz.util.IPUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.net.SocketException;


@RestController
@Slf4j
public class ProductController {
    @Autowired
    private ProductService productService;

    @RequestMapping("/product/{pid}")
    public Product queryProduct(@PathVariable("pid") Integer pid, HttpServletRequest request) throws SocketException {
        log.info("用户信息:"+JSONObject.toJSONString(UserInfoContext.getUser()));

        log.info("查询商品信息pid:" + pid);
        log.info("request ip:"+ IPUtil.getIpAddr(request));
        log.info("local ip:"+IPUtil.getRealIp());
        Product product = productService.findById(pid);
        log.info("获取到商品信息:" + JSONObject.toJSONString(product));


        return product;
    }

    @RequestMapping("/product/reduceInventory")
    public void reduceInventory(Integer pid,Integer num) {
        log.info("扣库存:pid" + pid+",num:"+num);
        productService.reduceInventory( pid, num);
    }
}
