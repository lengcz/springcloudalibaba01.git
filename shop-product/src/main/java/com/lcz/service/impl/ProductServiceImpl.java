package com.lcz.service.impl;

import com.lcz.dao.ProductDao;
import com.lcz.pojo.Product;
import com.lcz.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class ProductServiceImpl implements ProductService {

    @Autowired
    private ProductDao productDao;

    @Override
    public Product findById(Integer pid) {
        return productDao.findById(pid).get();
    }

    @Transactional  //事务注解
    @Override
    public void reduceInventory(Integer pid, Integer num) {
        Product product = productDao.findById(pid).get();
        product.setStock(product.getStock()-num);
        if(pid==3){
            int i=1/0;//模拟异常
        }
        productDao.save(product);
    }
}
