package com.lcz.service;

import com.lcz.pojo.Product;

public interface ProductService {

    Product findById(Integer pid);

    /**
     * 扣除库存
     * @param pid
     * @param num
     */
    void reduceInventory(Integer pid, Integer num);
}
