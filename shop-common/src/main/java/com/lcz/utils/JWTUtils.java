package com.lcz.utils;


import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTCreator;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.interfaces.DecodedJWT;

import java.util.Calendar;
import java.util.Map;

/**
 * JWT
 */
public class JWTUtils {

    /**
     * 密钥
     */
    private static final String SECRET = "FTIq]>h!>2H-zd_";

    /**
     * 生成token
     *
     * @param map
     * @param amount 有效期秒
     * @return
     */
    public static String createToken(Map<String, String> map, Integer amount) {
        Calendar instance = Calendar.getInstance();
        /**
         * 签名失效时间如果不传递，则默认7天
         */
        amount = (null == amount || amount < 1) ? 7 * 24 * 60 * 60 : amount;
        instance.add(Calendar.SECOND, amount);
        //默认包header加密是HS256，通常不需要设置
        JWTCreator.Builder builder = JWT.create();
        map.forEach((k, v) -> {
            builder.withClaim(k, v);
        });
        String token = builder.withExpiresAt(instance.getTime()) //指定过期时间
                .sign(Algorithm.HMAC256(SECRET));//签名
        return token;
    }

    /**
     * 验签
     *
     * @param token
     */
    public static void vertifyToken(String token) {
        getTokenInfo(token);
    }

    /**
     * 获取token信息
     *
     * @param token
     */
    public static DecodedJWT getTokenInfo(String token) {
        JWTVerifier jwtVerifier = JWT.require(Algorithm.HMAC256(SECRET)).build();
        DecodedJWT verify = jwtVerifier.verify(token);//解析jwt
        return verify;
    }
}
