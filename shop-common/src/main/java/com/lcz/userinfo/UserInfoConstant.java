package com.lcz.userinfo;

public class UserInfoConstant {

    public static final String CONSUMER="consumer";

    public static final String PROVIDER="provider";

    public static final String DUBBO_USER_KEY="DUBBO_USER_INFO";

    public static final String Authorization="Authorization";
}
