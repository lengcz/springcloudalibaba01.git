package com.lcz.userinfo;


import com.alibaba.dubbo.common.extension.Activate;
import com.alibaba.dubbo.rpc.*;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.rpc.Invocation;
import org.apache.dubbo.rpc.Invoker;
import org.apache.dubbo.rpc.Result;
import org.apache.dubbo.rpc.RpcException;

@Slf4j
@Activate(group = {UserInfoConstant.PROVIDER})
public class UserInfoProviderFilter implements org.apache.dubbo.rpc.Filter {

    @Override
    public Result invoke(Invoker<?> invoker, Invocation invocation) throws RpcException {
//        log.info("生成方:------");
        Object userInfo = invocation.getObjectAttachment(UserInfoConstant.DUBBO_USER_KEY);
        if (null != userInfo) {
            UserInfoContext.setUser((UserInfo) userInfo);
        }
        return invoker.invoke(invocation);
    }
}
