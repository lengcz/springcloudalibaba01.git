package com.lcz.userinfo;

import lombok.Data;

import java.io.Serializable;

@Data
public class UserInfo implements Serializable {
    private Integer uid; //用户id
    private String username;//用户名
}
