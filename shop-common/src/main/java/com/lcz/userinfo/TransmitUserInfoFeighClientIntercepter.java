package com.lcz.userinfo;

import com.alibaba.fastjson.JSON;
import feign.RequestInterceptor;
import feign.RequestTemplate;
import lombok.extern.slf4j.Slf4j;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;

@Slf4j
public class TransmitUserInfoFeighClientIntercepter implements RequestInterceptor {

    public TransmitUserInfoFeighClientIntercepter() {
    }

    @Override
    public void apply(RequestTemplate requestTemplate) {
        //从应用上下文中取出user信息，放入Feign的请求头中
        UserInfo user = UserInfoContext.getUser();
        log.info("传递用户信息:"+JSON.toJSONString(user));
        if (user != null) {
            try {
                String userJson = JSON.toJSONString(user);
                requestTemplate.header(UserInfoConstant.Authorization,new String[]{URLDecoder.decode(userJson,"UTF-8")});
            } catch (UnsupportedEncodingException e) {
                log.error("用户信息设置错误",e);
            }finally {
                UserInfoContext.remove();
            }
        }
    }
}