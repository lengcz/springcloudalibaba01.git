package com.lcz.userinfo;

import com.alibaba.dubbo.common.extension.Activate;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.rpc.*;

@Slf4j
@Activate(group = {UserInfoConstant.CONSUMER})
public class UserInfoConsumerFilter implements org.apache.dubbo.rpc.Filter {


    @Override
    public Result invoke(Invoker<?> invoker, org.apache.dubbo.rpc.Invocation invocation) throws RpcException {
//        log.info("消费方:------");
        try{
            UserInfo userInfo = UserInfoContext.getUser();
            if (null == userInfo) {
                return invoker.invoke(invocation);
            }
            invocation.getObjectAttachments().put(UserInfoConstant.DUBBO_USER_KEY, userInfo);
            return invoker.invoke(invocation);
        }finally {
            UserInfoContext.remove();
        }
    }
}
